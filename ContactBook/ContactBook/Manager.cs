﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactBook
{
    class Manager
    {
        public void Add()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(new string('_', 30));
            Console.ResetColor();
            Console.Write("Name : ");
            string name = Console.ReadLine();
            Console.Write("SurName : ");
            string surName = Console.ReadLine();
            Console.Write("Email : ");
            string email = Console.ReadLine();
            Console.Write("Number : ");
            string number = Console.ReadLine();
            Console.Write("Country : ");
            string country = Console.ReadLine();

            DataBase.contact.Add(new Contact {
                Name = name,
                SurName = surName,
                Email = email,
                Number = number,
                Country = country
            });
            if (!DataBase.contact[DataBase.contact.Count-1].IsValid)
            {
                Console.WriteLine("Something is invalid");
                DataBase.contact.RemoveAt(DataBase.contact.Count - 1);
                Add();
            }

        }

        public void Delete(int index)
        {
            DataBase.contact.RemoveAt(index);
        }

        public void Edit(int index)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(new string('_', 30));
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Old Data");
            Console.ResetColor();
            Print(index);

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(new string('_', 30));
            Console.ResetColor();
            Console.Write("Name : ");
            string name = Console.ReadLine();
            Console.Write("SurName : ");
            string surName = Console.ReadLine();
            Console.Write("Email : ");
            string email = Console.ReadLine();
            Console.Write("Number : ");
            string number = Console.ReadLine();
            Console.Write("Country : ");
            string country = Console.ReadLine();

            DataBase.contact.RemoveAt(index);

            DataBase.contact.Insert(index,new Contact
            {
                Name = name,
                SurName = surName,
                Email = email,
                Number = number,
                Country = country
            });
            if (!DataBase.contact[DataBase.contact.Count - 1].IsValid)
            {
                Console.WriteLine("Something is invalid");
                DataBase.contact.RemoveAt(DataBase.contact.Count - 1);
                Edit(index);
            }
        }

        public void Clear()
        {
            DataBase.contact.Clear();
        }

        public void Print()
        {
            var contacts = DataBase.contact;
            string title = "Name\t\t\tSurname\t\t\tEmail\t\t\tNumber\t\t\tCountry\n";
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(title);
            Console.ResetColor();

            for (int i = 0; i < contacts.Count; i++)
            {
                int quant = 0;
                Console.Write(contacts[i].Name);
                quant = ChooseQuant(contacts[i].Name);
                Console.Write(new string('\t',quant));

                Console.Write(contacts[i].SurName);
                quant = ChooseQuant(contacts[i].SurName);
                Console.Write(new string('\t', quant));

                Console.Write(contacts[i].Email);
                quant = ChooseQuant(contacts[i].Email);
                Console.Write(new string('\t', quant));

                Console.Write(contacts[i].Number);
                quant = ChooseQuant(contacts[i].Number);
                Console.Write(new string('\t', quant));

                Console.Write(contacts[i].Country);
                quant = ChooseQuant(contacts[i].Country);
                Console.Write(new string('\t', quant) + "\n");
                
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine(new string('*', 110));
                Console.ResetColor();
            }
        }

        private void Print(int i)
        {
            var contacts = DataBase.contact;
            string title = "Name\t\tSurname\t\tEmail\t\tNumber\t\tCountry\n";

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(title);
            Console.ResetColor();
            Console.WriteLine($"{contacts[i].Name}\t\t" +
                              $"{contacts[i].SurName}\t\t" +
                              $"{contacts[i].Email}\t\t" +
                              $"+{contacts[i].Number}\t\t" +
                              $"{contacts[i].Country}\t\t");
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine(new string('*', 75));
            Console.ResetColor();
        }

        private int ChooseQuant(string str)
        {
            int quant = 0;
            if (str.Length < 8)
            {
                quant = 3;
            }
            else if (str.Length < 16)
            {
                quant = 2;
            }
            else if (str.Length < 24)
            {
                quant = 1;
            }
            return quant;
        }
    }
}
