﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactBook
{
    class Contact
    {
        private string name;
        public string Name {
            get { return name; }
            set
            {
                if (string.IsNullOrEmpty(value) || value.Length > 22 || IsContainingNumber(value) || IsContainingSymbol(value))
                    IsValid = false;
                else
                {
                    name = RightGrammer(value);
                }
            }
        }

        private string surname;
        public string SurName
        {
            get { return surname; }
            set
            {
                if (string.IsNullOrEmpty(value) || value.Length > 22 || IsContainingNumber(value) || IsContainingSymbol(value))
                    IsValid = false;
                else
                {
                    surname = RightGrammer(value);

                }
            }
        }

        private string email;
        public string Email
        {
            get { return email; }
            set
            {
                if (string.IsNullOrEmpty(value) || value.Length > 22 || !IsContainingMySymbols(value))
                    IsValid = false;
                else
                    email = value;
            }
        }

        private string number;
        public string Number
        {
            get { return number; }
            set
            {
                if (string.IsNullOrEmpty(value) || value.Length > 22 || !int.TryParse(value, out int res) || res < 0)
                    IsValid = false;
                else               
                    number = res.ToString();                  
            }
        }

        private string country;
        public string Country
        {
            get { return country; }
            set
            {
                if (string.IsNullOrEmpty(value) || value.Length > 22 || IsContainingNumber(value) || IsContainingSymbol(value))
                    IsValid = false;
                else
                {
                    country = RightGrammer(value);

                }
            }
        }

        public bool IsValid = true;

        private bool IsContainingNumber(string value)
        {
            bool isNum = false;
            for (int i = 0; i < value.Length; i++)
            {
                if (char.IsDigit(value[i]))
                {
                    isNum = true;
                    break;
                }
            }
            return isNum;
        }

        private bool IsContainingSymbol(string value)
        {
            bool isSymbol = false;
            for (int i = 0; i < value.Length; i++)
            {
                if (char.IsSymbol(value[i]))
                {
                    isSymbol = true;
                    break;
                }
            }
            return isSymbol;
        }

        private bool IsContainingMySymbols(string value)
        {
            int dog = 0;
            bool isDot = false;
            for (int i = 0; i < value.Length; i++)
            {
                if (value[i] == '@')
                    dog++;                    
                if (value[i] == '.')
                    isDot = true;
            }


            if (dog == 1 && isDot)
                return true;
            else
                return false;
        }

        private string RightGrammer(string value)
        {
            StringBuilder build = new StringBuilder(value);
            build[0] = char.ToUpper(build[0]);
            for (int i = 1; i < value.Length; i++)
            {
                build[i] = char.ToLower(build[i]);
            }
            return build.ToString();
        }
    }
}
