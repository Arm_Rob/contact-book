﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactBook
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetWindowSize(150, 40);
            Manager mng = new Manager();

            Menu(mng);
        }
        static void Menu(Manager mng)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(new string('-', 20) + "CONTACTBOOK" + new string('-', 20));
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("###CLICK###");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(" \t1 to clear console");
            Console.WriteLine(" \t2 to add contact");
            Console.WriteLine(" \t3 to delete contact");
            Console.WriteLine(" \t4 to edit contact");
            Console.WriteLine(" \t5 to delete !all! contacts");
            Console.WriteLine(" \t6 to print contacts");

            Console.ResetColor();
            ConsoleKey key = Console.ReadKey(true).Key;

            if (key == ConsoleKey.D1 || key == ConsoleKey.NumPad1)
            {
                Console.Clear();
            }
            else if (key == ConsoleKey.D2 || key == ConsoleKey.NumPad2)
            {
                mng.Add();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"Adding succeded");
                Console.ResetColor();
            }
            else if (key == ConsoleKey.D3 || key == ConsoleKey.NumPad3)
            {
                Delete:
                int last = DataBase.contact.Count - 1;
                if (last == -1)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"No Contacts!!!");
                    Console.ResetColor();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write($"Which one (0-{last}) /to quite write \"cancel\"/");
                    Console.ResetColor();
                    string resStr = Console.ReadLine();
                    if (resStr != "cancel")
                    {
                        if (int.TryParse(resStr, out int res))
                        {
                            if (res > last || res < 0)
                            {
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine($"Choose Sometinhg from 0 - {last}");
                                Console.ResetColor();
                                goto Delete;
                            }
                            else
                            {
                                mng.Delete(res);
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine($"Deleting succeded");
                                Console.ResetColor();
                            }
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine($"Not a Number");
                            Console.ResetColor();
                            goto Delete;
                        }
                    }
                }
            }
            else if (key == ConsoleKey.D4 || key == ConsoleKey.NumPad4)
            {
                Edit:
                int last = DataBase.contact.Count - 1;
                if (last == -1)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"No Contacts!!!");
                    Console.ResetColor();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write($"Which one (0-{last}) /to quite write \"cancel\"/");
                    Console.ResetColor();
                    string resStr = Console.ReadLine();
                    if (resStr != "cancel")
                    {
                        if (int.TryParse(resStr, out int res))
                        {
                            if (res > last || res < 0)
                            {
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine($"Choose Sometinhg from 0 - {last}");
                                Console.ResetColor();
                                goto Edit;
                            }
                            else
                            {
                                mng.Edit(res);
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine($"Edding succeded");
                                Console.ResetColor();
                            }
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine($"Not a Number");
                            Console.ResetColor();
                            goto Edit;
                        }
                    }
                }
            }
            else if (key == ConsoleKey.D5 || key == ConsoleKey.NumPad5)
            {
                mng.Clear();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"Clearing succeded");
                Console.ResetColor();
            }
            else if (key == ConsoleKey.D6 || key == ConsoleKey.NumPad6)
            {
                mng.Print();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Choose Sometinhg from 1 - 6");
                Console.ResetColor();
            }

            Menu(mng);
        }
    }
}
